/** @type {import('postcss-load-config').Config} */

const config = {
  plugins: {
    // must be first
    "postcss-import": {},
    "tailwindcss/nesting": {},
    tailwindcss: {},
    autoprefixer: {},
  },
};

export default config;
